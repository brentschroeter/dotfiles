alias ll="ls -alG"
alias py='python3'
alias attu='ssh brentsch@attu.cs.washington.edu'
alias c='clear'
alias dirt='docker run -it --rm'
alias drum='docker run --rm'
alias vim='nvim'
alias oldvim="$(which vim)"
function mkd {
  mkdir -p "${@}" && cd "${@}"
}

if [ -s ~/.grog.bash ]; then
  # https://gitlab.com/brentschroeter/grog
  source ~/.grog.bash
fi

[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion

if (ifconfig | grep -E "^en0:" > /dev/null); then
  HOST_ADDR=$(ifconfig en0 | grep inet | grep -v inet6 | awk '{print $2}')
elif (ifconfig | grep -E "^wlan0:" > /dev/null); then
  HOST_ADDR=$(ifconfig wlan0 | grep inet | grep -v inet6 | awk '{print $2}')
else
  HOST_ADDR="\h"
fi
export PS1="\u@${HOST_ADDR} \[\033[01;36m\]\w\[\033[0m\] $ "

if command -v tmux > /dev/null; then
  [ -z "$TMUX" ] && exec tmux
fi
