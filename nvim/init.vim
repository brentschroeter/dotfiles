set nocompatible
set termguicolors

filetype plugin indent on

" Performance
set lazyredraw
hi NonText cterm=NONE ctermfg=NONE
set scrolljump=5
set ttyfast
set autoread
set clipboard=unnamed
set updatetime=100

" Disable auto-formatting for Firestore .rules files.
autocmd BufNewFile,BufRead *.rules set filetype=text

" Fix ftdetect for .tsx files.
autocmd BufNewFile,BufRead *.tsx set filetype=typescript.tsx

" Key Mappings
let mapleader="\<Space>"
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader>e :e<Space>
nnoremap <Leader><Leader> <C-w><C-w>
nnoremap <Leader>v :vs<Space>
nnoremap <Leader>s :sp<Space>
nnoremap <Leader>o <C-w><C-r>
nnoremap <silent> <Leader><Up> :exe "res +5"<CR>
nnoremap <silent> <Leader><Down> :exe "res -5"<CR>
nnoremap <silent> <Leader><Right> :exe "vertical res +5"<CR>
nnoremap <silent> <Leader><Left> :exe "vertical res -5"<CR>
nnoremap <silent> <CR> :noh<CR>
nnoremap <Leader>t :tabnew<CR>
nnoremap <Leader>n :tabnext<CR>
nnoremap <Leader>rc :vs $MYVIMRC<CR>
nnoremap <Leader>rs :source $MYVIMRC<CR>
nnoremap gg m'gg
nnoremap G m'G

" Indentation
set et
set tabstop=2
set shiftwidth=2
set autoindent

" Editor Layout
set cc=80,100
set lazyredraw
set splitbelow
set splitright
set nu
" set relativenumber
set incsearch
set mouse=a
let g:netrw_liststyle=3 " Open explorer in tree view.
let g:netrw_silent=1 " Suppress messages when saving via SCP.
" Backup Files
set backup
if !isdirectory($HOME . '/.config/nvim/backups')
  silent exec '!mkdir ~/.config/nvim/backups'
endif
set backupdir=~/.config/nvim/backups//
set dir=~/.config/nvim/backups//

" Colors
syntax on
if filereadable($HOME . '/.config/nvim/colors/molokai_black.vim')
  colorscheme molokai_black
  set cursorline
  set cursorcolumn
endif

" Install and source plugins if Vim-Plug is installed.
if filereadable($HOME . "/.config/nvim/autoload/plug.vim")
  call plug#begin()

  " Indentation detection.
  Plug 'tpope/vim-sleuth'
  " Deoplete for completion.
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
  " Plug 'neoclide/coc.nvim', { 'branch': 'release' }
  " ALE for linting.
  Plug 'dense-analysis/ale'
  " {Save( my [fingers])}.
  Plug 'jiangmiao/auto-pairs'
  " Airline, to make it look beautiful and tell me which mode I'm in.
  Plug 'vim-airline/vim-airline'
  " CtrlP for fuzzy opening buffers.
  " Plug 'kien/ctrlp.vim'
  " Git Gutter, to remind me to commit when I'm in the zone.
  " This has been replaced by vim-signify, below.
  " Plug 'airblade/vim-gitgutter'
  " Plug 'mhinz/vim-signify'
  " Vim-Expand-Region--I rarely need it but get annoyed if it's missing.
  Plug 'terryma/vim-expand-region'
  " All my Go stuff...
  Plug 'fatih/vim-go'
  " Plug 'zchee/deoplete-go'
  " Python autocompletion
  Plug 'deoplete-plugins/deoplete-jedi'
  " Nginx syntax highlighting.
  Plug 'chr4/nginx.vim'
  " I've never noticed problems with Tmux focus, but seems like best practice.
  Plug 'tmux-plugins/vim-tmux-focus-events'
  " Julia language support.
  Plug 'JuliaEditorSupport/julia-vim'
  " Reason language support.
  " Plug 'reasonml-editor/vim-reason-plus'
  Plug 'jparise/vim-graphql'
  Plug 'hashivim/vim-terraform'

  call plug#end()

  " Expand-Region
  vmap v <Plug>(expand_region_expand)
  vmap V <Plug>(expand_region_shrink)

  " Deoplete
  let g:deoplete#enable_at_startup = 1
  inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
  set completeopt-=preview
"  inoremap <silent><expr> <TAB>
"    \ pumvisible() ? "\<C-n>" :
"    \ <SID>check_back_space() ? "\<TAB>" :
"    \ coc#refresh()
"  inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

  function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
  endfunction

  " Use <c-space> to trigger completion.
"  if has('nvim')
"    inoremap <silent><expr> <c-space> coc#refresh()
"  else
"    inoremap <silent><expr> <c-@> coc#refresh()
"  endif

  " Airline
  if !exists('g:airline_symbols')
    let g:airline_symbols = {}
  endif
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
  let g:airline#extensions#ale#enabled = 1
  set laststatus=2

  " ALE
  nnoremap <silent> cc :ALEPrevious<CR>
  nnoremap <silent> cv :ALENext<CR>
  let g:ale_sign_error = '⤫'
  let g:ale_sign_warning = '⚠'
  " Prevent the help window from popping up and stealing focus.
  let g:ale_open_list = 0
  " Do basic fixes automatically.
  let g:ale_fix_on_save = 1
  let g:ale_linters = {
  \   'typescript': ['eslint', 'tsserver'],
  \   'typescript.tsx': ['eslint', 'tsserver'],
  \   'go': ['golangci-lint'],
  \}
  let g:ale_fixers = {
  \   '*': ['remove_trailing_lines', 'trim_whitespace'],
  \}
  " \   'typescript': ['eslint'],
  " \   'typescript.tsx': ['eslint'],
  " Disable entirely for Java.
  let g:ale_pattern_options = {
  \   '.*\.java$': {'ale_enabled': 0},
  \}
  " Resolve names defined across files in a Go package.
  let g:ale_go_golangci_lint_package = 1

  " Git Gutter
  " let g:gitgutter_map_keys = 0

  " CtrlP
  nnoremap <Leader>p :CtrlP<CR>
  let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git\|dist'

  " Vim-Go
  nnoremap <silent> <Leader>gi :GoInfo<CR>
  au BufRead,BufNewFile *.jsm set filetype=javascript
  au FileType go set noexpandtab
  au FileType go set shiftwidth=4
  au FileType go set softtabstop=4
  au FileType go set tabstop=4
  let g:go_highlight_build_constraints = 1
  let g:go_highlight_extra_types = 1
  let g:go_highlight_fields = 1
  let g:go_highlight_functions = 1
  let g:go_highlight_methods = 1
  let g:go_highlight_operators = 1
  let g:go_highlight_structs = 1
  let g:go_highlight_types = 1
  let g:go_fmt_command = "goimports"
  let g:go_auto_type_info = 0
  let g:go_auto_sameids = 0
  let g:go_addtags_transform = "snakecase"
  let g:go_list_height = 0
  let g:go_list_type = ""
  let g:go_template_autocreate = 1
  let g:go_fmt_fail_silently = 1
endif
