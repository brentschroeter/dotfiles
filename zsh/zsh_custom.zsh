export PATH="/usr/local/bin:$PATH"

if [[ -z "$TMUX" ]] && (command -v tmux > /dev/null 2>&1); then
  tmux
fi

alias ll="ls -alG"
alias py='python3'
alias c='clear'
alias dirt='docker run -it --rm'
alias drum='docker run --rm'
alias vim='nvim'
alias oldvim="$(which vim)"
function mkd {
  mkdir -p "${@}" && cd "${@}"
}
function attu {
  if [[ "$#" == '0' ]]; then
    ssh brentsch@attu.cs.washington.edu
  elif [[ "$#" == '1' ]]; then
    ssh "brentsch@attu${1}.cs.washington.edu"
  else
    ssh "${2}@attu${1}.cs.washington.edu"
  fi
}

if [ -s ~/.grog.bash ]; then
  # https://gitlab.com/brentschroeter/grog
  source ~/.grog.bash
fi

if [ -s /usr/local/share/zsh-completions ]; then
  export fpath=(/usr/local/share/zsh-completions $fpath)
fi

export ZSH_THEME=brents

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/brent/developer/google-cloud-sdk/path.zsh.inc' ]; then source '/Users/brent/developer/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/brent/developer/google-cloud-sdk/completion.zsh.inc' ]; then source '/Users/brent/developer/google-cloud-sdk/completion.zsh.inc'; fi
