#!/bin/bash

set -eufxo pipefail

username='brent'

cd "$(dirname $0)"

apt-get update
apt-get install -y neovim zsh

if ! (id "$username" &>/dev/null); then
  addgroup "$username"
  adduser --shell "$(which zsh)" --ingroup "$username" "$username"
  usermod -aG sudo $username
fi

if [ ! -f "/home/$username/.config/nvim" ]; then
  mkdir -p "/home/$username/.config"
  cp -r nvim "/home/$username/.config/nvim"
  chown -R "$username:$username" "/home/$username/.config"
  chmod -R 700 "/home/$username/.config"
fi

if [ ! -f "/home/$username/.zshrc" ]; then
  cp ./zsh/basic.zshrc "/home/$username/.zshrc"
  chown "$username:$username" "/home/$username/.zshrc"
  chmod 600 "/home/$username/.zshrc"
fi

if [ ! -f "/home/$username/.oh-my-zsh" ]; then
  su brent -c 'sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"'
fi
