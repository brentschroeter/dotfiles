#!/bin/sh

pkg_mgr='apt-get'

$pkg_mgr update
$pkg_mgr install neovim

mkdir -p ~/.config

# Assumes that ~/.config/nvim/ does not exist.
cp -r nvim ~/.config/nvim
